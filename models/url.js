var mongoose = require('mongoose');
var counter = mongoose.model('counter');
var Schema = mongoose.Schema;

var UrlSchema = new Schema({
	_key : {type : Number, require : true, unique : true},
	longurl : {type : String, require : true},
	created_at : Date
});

UrlSchema.pre('save',function (next){
	var doc = this;
	counter.findByIdAndUpdate({_id: 'urls'}, {$inc: {seq: 1}}, function(error, counter) {
      if (error){
      	return next(error);
      }
      doc._key = counter.seq;
      doc.created_at = new Date();
      next();
  });
});

module.exports = mongoose.model('Url',UrlSchema);