function getUrl(){
  var longurl = $("#TxtBoxLongUrl").val();
  $.getJSON("http://micro-url.herokuapp.com/api/new/"+longurl,function (data){
    if (data.hasOwnProperty('error')){
      alert(data.error);
    }
    else{
      var shorturl = data.short_url; 
      $("#TxtBoxShortUrl").val(shorturl);
    }
  });
};
