var mongoose   = require('mongoose');
var config = require('./config');
var dbURI = config.db.connstring;
mongoose.connect(dbURI); // connect to our database
//Callback For connection events (mostly for logging purpose)

//Connected
mongoose.connection.on('connected', function () {  
  console.log('Mongoose connected');
}); 

//Error
mongoose.connection.on('error',function (err) {  
  console.log('Mongoose got error : ' + err);
}); 

//Disconnected
mongoose.connection.on('disconnected', function () {  
  console.log('Mongoose disconnected'); 
});

//Node process terminated
process.on('SIGINT', function() {  
  mongoose.connection.close(function () { 
    console.log('Disconnect mongoose'); 
    process.exit(0); 
  }); 
}); 

require('./models/counter');
require('./models/url');