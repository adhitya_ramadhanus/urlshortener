var express = require('express');
var router = express.Router();
var status = require('http-status');
var config = require('../config');
var encoder = require('../helpers/encoders');

var db = require('../db');
var mongoose = require('mongoose'), Urls = mongoose.model('Url');
var apicache = require('apicache').options({ debug: true }).middleware;

router.get("/",function (req,res){
	res.render('index', { title: 'Micro URL'});
});

router.route('/api/new/*')
	.get(apicache('5 minutes'),function (req,res){
		var newurl = req.params[0];
		var regexp = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/; 
		var matched = newurl.match(regexp);
		console.log(matched);
		if (matched != null){
			Urls.findOne({longurl : newurl},function (err,url){
			if (err){
				return res
          			.status(status.INTERNAL_SERVER_ERROR)
          			.json({ error : err.toString()});
			}
			else if (url){
				res.status(200).json({original_url : newurl,short_url : config.endpoint+encoder.encode(url._key)});
			}
			else{
				Urls({longurl : newurl}).save(function (err,url){
					if (err){
						return res
          					.status(status.INTERNAL_SERVER_ERROR)
          					.json({ error : err.toString()});
					}
					res.status(201).json({original_url : newurl,short_url : config.endpoint+encoder.encode(url._key)});
				});
			}
			});
		}
		else{
			res.json({error : "Please input only URL"});
		}
	});
router.route('/:key')
	.get(function (req,res){
		Urls.findOne({_key : encoder.decode(req.params.key)},function (err,url){
			if (err){
				return res
          		.status(status.INTERNAL_SERVER_ERROR)
          		.json({ error : err.toString()});
			}
			if (!url){
				return res
					.status(status.NOT_FOUND)
					.json({error : "Url Not Found"});
			}
			console.log("REDIRECTED TO "+url.longurl);
			res.status(302).redirect(url.longurl);
		});
	})
	.delete(function (req,res){
		Urls.findOne({_key : encoder.decode(req.params.key)}).remove(function (err){
			if (err){
				return res
          		.status(status.INTERNAL_SERVER_ERROR)
          		.json({ error : err.toString()});
			}
			res.json({message : "Urls succesfully deleted!"});
		})
	});

module.exports = router;