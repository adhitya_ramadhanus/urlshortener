var config = {};

config.db = {};
config.db.connstring = process.env.MONGOLAB_URI;

config.endpoint= "http://micro-url.herokuapp.com/";

module.exports = config;