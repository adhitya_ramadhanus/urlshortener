var config = require('../config');
var mongoose = require('mongoose');
var should = require('should'); 
var assert = require('assert');
var supertest = require('supertest');

var server = supertest.agent(config.endpoint);
describe('Urls Endpoint', function() {
  it('Can short url', function(done) {
    server
      .get('new/http://www.xtremax.com')
      .end(function(err, res) {
        if (err) {
          throw err;
        }
        res.status.should.equal(201);
        res.body.original_url.should.equal('http://www.xtremax.com');
        done();
      });
    });
  it('should not create same shortened url', function(done) {
    server
      .get('new/http://www.xtremax.com')
      .end(function(err, res) {
        if (err) {
          throw err;
        }
        res.status.should.equal(200);
        res.body.original_url.should.equal('http://www.xtremax.com');
        done();
      });
    });
  it('should not create same shortened url (cached)', function(done) {
    server
      .get('new/http://www.xtremax.com')
      .end(function(err, res) {
        if (err) {
          throw err;
        }
        res.status.should.equal(200);
        res.body.original_url.should.equal('http://www.xtremax.com');
        done();
      });
    });
});